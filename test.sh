#!/bin/sh
# Give in the command to test against as argument(s)
# Path needs to be absolute!
# Examples: ./test.sh readlink -f
#           ./test.sh cat
#           ./test.sh $PWD/otherscript.sh

bin="$@"
[ -z "$bin" ] && bin=$PWD/realpath.sh

dir="$(mktemp -d)"
cd "$dir"

do_test() {
  name=$2
  should="$3:$4"

  is=$($bin "$name")
  is="$is:$?"

  if [ "$is" = "$should" ]; then
    echo "Test $1 success"
  else
    echo "Test $1 fail: \"${bin##*/} $name\" returned \"$is\" instead of \"$should\""
    #stat "$name"
  fi
}

touch file
mkdir dir
ln -s dir dir_symlink
ln -s file file_symlink
ln -s none invalid_symlink
ln -s . dir/samedir
ln -s .. dir/parent

do_test 1 file "$dir"/file 0
do_test 2 dir "$dir"/dir 0
do_test 3 dir_symlink "$dir"/dir 0
do_test 4 file_symlink "$dir"/file 0
do_test 5 invalid_symlink "" 1
do_test 6 invalid_symlink/fail "" 1
do_test 7 dir/samedir "$dir"/dir 0
do_test 8 dir/parent "$dir" 0

do_test 9 "$dir"/file "$dir"/file 0
do_test 10 "$dir"/dir "$dir"/dir 0
do_test 11 "$dir"/dir_symlink "$dir"/dir 0
do_test 12 "$dir"/file_symlink "$dir"/file 0
do_test 13 "$dir"/invalid_symlink "" 1
do_test 14 "$dir"/invalid_symlink/fail "" 1
do_test 15 "$dir"/dir/samedir "$dir"/dir 0
do_test 16 "$dir"/dir/parent "$dir" 0

rm -rf "$dir"
